build : 
	cargo build --release

install :
	cp target/release/daemon /usr/bin/knockd_rs
	mkdir /etc/knock_rs/
	cp config/* /etc/knock_rs/
	cp target/release/client /usr/bin/knock_rs

uninstall :
	rm /usr/bin/knock_rs
	rm -rf /etc/knock_rs/
