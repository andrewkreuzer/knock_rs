use packet_parser;

use criterion::{criterion_group, criterion_main, Criterion};

fn criterion_benchmark(c: &mut Criterion) {
    // let mut file = OpenOptions::new()
    //     .write(true)
    //     .append(true)
    //     .open("sample_packets")
    //     .unwrap();

    // let packet_string: String = packet
    //     .into_iter()
    //     .map(|p| p.to_string())
    //     .collect::<Vec<String>>()
    //     .join(", ");

    // if let Err(e) = writeln!(file, "{}", packet_string) {
    //     eprintln!("Couldn't write to file: {}", e);
    // }
    // Ipv4 packet
    let packet: [u8; 698] = [
        // 8, 0, 39, 214, 201, 86, 64, 22, 126, 119, 31, 97, 8, 0, 69, 0, 0, 52, 59, 211, 64, 0,
        // 128, 6, 57, 205, 192, 168, 1, 218, 192, 168, 1, 249, 201, 250, 31, 144, 233, 35, 65,
        // 92, 0, 0, 0, 0, 128, 2, 32, 0, 181, 225, 0, 0, 2, 4, 5, 180, 1, 3, 3, 8, 1, 1, 4, 2,
        
        64, 22, 126, 119, 31, 97, 228, 244, 198, 2, 203, 156, 8, 0, 69, 0, 2, 172, 111, 151, 0, 0,
        118, 6, 12, 213, 35, 186, 224, 25, 192, 168, 1, 100, 1, 187, 155, 240, 35, 70, 158, 16, 59,
        123, 6, 59, 128, 24, 20, 29, 192, 42, 0, 0, 1, 1, 8, 10, 75, 179, 151, 189, 106, 59, 174,
        29, 23, 3, 3, 2, 115, 59, 97, 30, 232, 161, 61, 122, 226, 246, 37, 184, 177, 66, 189, 210,
        199, 177, 166, 114, 7, 195, 100, 150, 71, 45, 54, 183, 23, 115, 238, 159, 56, 130, 251,
        143, 221, 120, 16, 208, 226, 118, 197, 167, 68, 212, 135, 74, 75, 170, 238, 123, 180, 224,
        240, 113, 136, 8, 240, 163, 194, 71, 183, 83, 224, 106, 126, 101, 232, 54, 156, 68, 205,
        76, 20, 192, 208, 112, 190, 156, 165, 93, 214, 218, 245, 195, 96, 207, 84, 243, 224, 138,
        192, 103, 67, 63, 195, 108, 45, 214, 254, 231, 110, 163, 120, 155, 201, 16, 120, 33, 65,
        120, 3, 246, 117, 57, 222, 147, 71, 64, 181, 235, 23, 28, 13, 177, 188, 171, 216, 27, 209,
        125, 72, 93, 101, 174, 50, 35, 223, 41, 131, 40, 95, 18, 21, 183, 78, 47, 156, 2, 157, 198,
        159, 99, 149, 38, 13, 19, 90, 51, 85, 223, 40, 128, 117, 229, 15, 121, 181, 126, 86, 178,
        152, 208, 178, 112, 44, 227, 159, 46, 113, 107, 92, 255, 43, 205, 68, 173, 86, 83, 159, 2,
        187, 19, 236, 237, 195, 101, 243, 189, 142, 61, 112, 20, 81, 218, 87, 190, 100, 21, 76, 15,
        129, 245, 49, 23, 148, 66, 115, 132, 18, 37, 98, 119, 57, 191, 5, 210, 204, 236, 184, 140,
        35, 137, 162, 185, 97, 184, 72, 204, 157, 156, 196, 58, 185, 140, 99, 107, 186, 192, 90,
        199, 250, 77, 6, 250, 185, 64, 206, 57, 253, 138, 134, 223, 197, 38, 94, 65, 164, 148, 176,
        11, 30, 3, 227, 14, 103, 4, 54, 234, 57, 68, 115, 156, 129, 206, 229, 153, 49, 73, 129, 70,
        68, 164, 172, 131, 42, 135, 172, 46, 56, 113, 167, 92, 89, 163, 89, 236, 96, 81, 214, 120,
        119, 8, 210, 97, 34, 213, 32, 186, 221, 188, 228, 242, 37, 215, 58, 60, 240, 20, 19, 212,
        203, 89, 159, 134, 54, 133, 185, 214, 67, 151, 168, 200, 172, 69, 26, 57, 121, 61, 204,
        101, 62, 163, 15, 145, 138, 102, 13, 26, 42, 218, 192, 205, 195, 248, 29, 135, 27, 97, 209,
        146, 173, 21, 133, 55, 224, 198, 27, 124, 241, 115, 123, 0, 93, 20, 167, 71, 38, 121, 16,
        184, 91, 54, 250, 171, 130, 140, 240, 146, 213, 9, 78, 230, 250, 192, 219, 92, 195, 12,
        201, 145, 218, 98, 55, 159, 251, 173, 48, 14, 175, 130, 191, 201, 12, 29, 65, 48, 20, 139,
        15, 227, 153, 202, 89, 32, 120, 115, 69, 220, 190, 129, 145, 169, 72, 185, 68, 29, 175,
        217, 99, 170, 95, 8, 71, 34, 33, 130, 209, 47, 106, 110, 173, 250, 19, 164, 44, 30, 81,
        188, 170, 27, 126, 216, 205, 203, 106, 165, 4, 91, 114, 104, 79, 168, 154, 85, 22, 121,
        164, 37, 5, 98, 127, 46, 17, 253, 201, 136, 27, 154, 248, 32, 172, 148, 2, 250, 170, 183,
        172, 22, 187, 106, 70, 46, 206, 93, 61, 250, 234, 132, 201, 171, 27, 26, 224, 142, 200,
        188, 175, 245, 120, 106, 25, 113, 237, 66, 62, 8, 211, 252, 131, 25, 207, 111, 71, 213, 31,
        111, 176, 227, 252, 194, 204, 87, 246, 55, 137, 218, 133, 242, 231, 87, 139, 140, 114, 211,
        214, 209, 69, 209, 9, 137, 177, 222, 192, 201, 14, 37, 139, 192, 216, 123, 122, 60, 234,
        199, 173, 239, 10, 9, 210, 161, 133, 68, 251, 117, 231, 72, 42, 18, 187, 45, 204, 212, 162,
        24, 33, 235, 234, 30, 10, 46, 179, 116, 158, 85, 20, 93, 224, 12, 137, 69, 57, 165, 119,
        46, 239, 57
    ];

    c.bench_function("Packets", |b| b.iter(|| packet_parser::parse_packet(&packet) ));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
