use std::error;
use std::fs::{self, OpenOptions};
use std::io::{self, prelude::*};
use rand::Rng;
use std::net::{TcpStream, UdpSocket};
use clap::{App, Arg};

fn seq_send(recipient: &str, ports: Vec<u16>, proto: &str) -> Result<(), Box<dyn error::Error>> {
    for port in ports {
        if proto == "tcp" {
            TcpStream::connect((recipient, port)).ok();
        } else {
            let buf = &[80];
            let socket = UdpSocket::bind(("0.0.0.0", 0)).unwrap();
            socket.send_to(buf , (recipient, port)).ok();
        }
    }
    Ok(())
}

fn get_otp_sequence() -> Option<Vec<u16>> {
    let file = fs::OpenOptions::new()
        .write(true)
        .read(true)
        .open("./config/otp_sequences.txt")
        .expect("unable to open file");

    let mut reader = io::BufReader::new(file);

    let mut line = String::new();
    reader.read_line(&mut line).expect("failed to read line from otp sequences");
    let seq_string = line.split(',').map(|s| { 
        s.chars()
            .filter(|c| !c.is_whitespace())
            .collect()
    }).collect::<Vec<String>>();
    let ports: Vec<u16> = seq_string.into_iter().map(|s| s.parse::<u16>().unwrap()).collect();

    let mut buf = Vec::new();
    reader.read_to_end(&mut buf).expect("unable to read to end of otp sequences");
    let mut file = reader.into_inner();
    file.seek(io::SeekFrom::Start(0)).expect("seek failed");
    file.write_all(&buf).expect("unable to upate otp file");

    Some(ports)
}


fn generate_otp_sequence() -> io::Result<()> {
    let mut rng = rand::thread_rng();
    let mut file = OpenOptions::new()
        .write(true)
        .create(true)
        .open("otp_sequences.txt")
        .expect("unable to open file");
    for _ in 0..1000/*length of otp sequences generated*/ {
        let mut seq: Vec<String> = Vec::new();
        for _ in 0..5 {
            let rnum = rng.gen_range(500, 65535).to_string();
            seq.push(rnum);
        }
        file.write_all(seq.join(", ").as_ref())?;
        file.write_all(b"\n")?;
    }
    Ok(())
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let matches = App::new("knocker")
        .subcommand(
            App::new("otp")
            .arg(
                Arg::new("generate")
                .short('g')
            )
            .arg(
                Arg::new("use")
                .short('u')
            )
        )
        .arg(
            Arg::new("recipient")
            .takes_value(true)
            .short('r'),
        )
        .arg(
            Arg::new("protocol")
            .short('p')
            .default_value("tcp")
            .takes_value(true),
        )
        .arg(
            Arg::new("sequence")
            .short('s')
            .multiple(true)
        )
        .get_matches();

    let mut seq = None;
    if let Some(otp) = matches.subcommand_matches("otp") {
        if otp.is_present("generate") {
            generate_otp_sequence()?;
            return Ok(());
        }
        if otp.is_present("use") {
            println!("using otp");
            seq = get_otp_sequence();
        }
    } else {
        seq = Some(matches.values_of_t("sequence").expect("no sequence"));
    }

    let recipient: &str = matches.value_of("recipient").expect("no recipient specified");
    let proto: &str = matches.value_of("protocol").unwrap_or("tcp");
    let seq = seq.expect("no sequence");

    seq_send(recipient, seq, proto)?;

    Ok(())
}
