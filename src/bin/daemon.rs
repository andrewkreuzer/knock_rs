use knock_rs;

use std::error;
use std::env;

fn main() -> Result<(), Box<dyn error::Error>> {
    let args: Vec<String> = env::args().collect();
    let default = "lo".to_string();
    let interface = args.get(1).unwrap_or(&default);

    knock_rs::main(interface)?;

    Ok(())
}
