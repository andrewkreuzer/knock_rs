pub mod state;
pub mod sec;
pub mod packet;

use std::error;
use std::sync::{Arc, Mutex};

use log::info;
use packet::capture;

pub fn main(interface: &str) -> Result<(), Box<dyn error::Error>>{

    // NOTE: libseccomp-dev needed for compile
    #[cfg(feature = "seccomp")]
    {
        if let Err(err) = sec::setup() {
            panic!("sec setup failed: {}", err);
        }
    }

    let state = Arc::new(Mutex::new(state::State::new()?));

    ctrlc::set_handler(||
        state::ctrlc_handle()
    ).expect("ctrlc handler failed to create termination hooks");

    info!("Starting capture");
    capture::start(interface, state.clone())?;

    Ok(())
}
