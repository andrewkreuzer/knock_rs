use crate::state::{self, State};

use std::sync::{Arc, Mutex};
use std::error;

use futures::StreamExt;
use pcap::stream::{PacketCodec, PacketStream};
use pcap::{Active, Capture, Error, Packet};

use packet_parser::{
    PacketInfo,
    parse_packet,
};

pub struct SimpleDumpCodec;

impl PacketCodec for SimpleDumpCodec {
    type Type = PacketInfo;

    fn decode<'p>(&mut self, packet: Packet<'p>) -> Result<Self::Type, Error> {

        if let Ok(packet_info) = parse_packet(packet.data) {
            return Ok(packet_info);
        }

        Err(Error::PcapError("incomplete packet".to_string()))
    }
}

fn new_stream(interface: &str) -> Result<PacketStream<Active, SimpleDumpCodec>, Error> {

    let cap = Capture::from_device(interface)?
        .immediate_mode(true)
        .open()?
        .setnonblock()?;
    cap.stream(SimpleDumpCodec {})
}

pub fn start(interface: &str, state: Arc<Mutex<State>>) -> Result<(), Box<dyn error::Error>> {
    let mut rt = tokio::runtime::Builder::new()
        .enable_io()
        .build()?;

    if let Ok(stream) = rt.enter(|| new_stream(interface)) {
        let fut = stream.for_each(move |packet_info| {
            match packet_info {
                Err(_) => {
                    futures::future::ready(())
                },
                Ok(packet_info) => {
                    state::update::update(packet_info, state.clone()).expect("update failed");
                    futures::future::ready(())
                }
            }
        });

        rt.block_on(fut);
    }

    Ok(())
}
