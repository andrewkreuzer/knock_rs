use std::error;

use log::{info, error};

pub struct Ipt {
    pub ipt: iptables::IPTables,
    pub ports: Vec<u8>,
}

impl Ipt {
    pub fn new() -> Result<Ipt, Box<dyn error::Error>> {
        // TODO: this false var unsets ipv6 support and will need to be 
        //       implemented for ipv6 support
        let ipt = iptables::new(false)?;

        info!("Starting initial iptables rules");
        match ipt.new_chain("filter", "KNOCK_RS") {
            Ok(_) => info!("Iptables chain created"),
            Err(e) => error!("Chain creation failed: {}", e),
        }

        // Flush if chain alread present
        ipt.flush_chain("filter", "KNOCK_RS")?;
        match ipt.append("filter", "KNOCK_RS", "-p tcp --dport 22 -j DROP") {
            Ok(_) => info!("SSH blocked"),
            Err(e) => error!("Blocking port 22 failed: {}", e),
        }

        if !(ipt.exists("filter", "INPUT", "-j KNOCK_RS")?) {
            match ipt.append("filter", "INPUT", "-j KNOCK_RS") {
                Ok(_) => info!("KNOCK_RS added to INPUT chain"),
                Err(e) => error!("Failed to add KNOCK_RS to INPUT chain: {}", e)
            }
        }

        info!("Done initial iptables rules");

        Ok(Ipt {
            ipt,
            ports: Vec::new(), 
        })
    }

    pub fn cleanup_iptables_rules(&self) -> Result<(), Box<dyn error::Error>> {
        info!("Removing all KNOCK_RS iptables rules");

        match self.ipt.delete_all("filter", "INPUT", "-j KNOCK_RS") {
            Ok(_) => info!("Removed KNOCK_RS from INPUT"),
            Err(e) => error!("Failed to remove KNOCK_RS from INPUT: {}", e),
        }

        if self.ipt.chain_exists("filter", "KNOCK_RS")? {
            self.ipt.flush_chain("filter", "KNOCK_RS")?;
            match self.ipt.delete_chain("filter", "KNOCK_RS") {
                Ok(_) => info!("KNOCK_RS chain deleted"),
                Err(e) => error!("Blocking port 22 failed: {}", e),
            }
        }

        info!("Iptables rules removed");

        Ok(())
    }

    pub fn open_sesame(&self, allowed_ip: &str) -> Result<(), Box<dyn error::Error>> {
        info!("Opening the gates");

        self.ipt.flush_chain("filter", "KNOCK_RS")?;
        self.ipt.append(
            "filter", 
            "KNOCK_RS", 
            &format!(
                "-p tcp -s {} --dport 22 -j ACCEPT", 
                allowed_ip)
            )?;

        self.ipt.append("filter", "KNOCK_RS", "-p tcp --dport 22 -j DROP")?;

        info!("Gates opened");

        Ok(())
    }

    pub fn close_the_gates(&self) -> Result<(), Box<dyn error::Error>>  {
        info!("Ending the session");

        self.ipt.flush_chain("filter", "KNOCK_RS")?;

        match self.ipt.append("filter", "KNOCK_RS", "-p tcp --dport 22 -j DROP") {
            Ok(_) => info!("SSH blocked"),
            Err(e) => error!("blocking port 22 failed: {}", e),
        }
        info!("Session closed");

        Ok(())
    }

}

// ctrlc_handle seems to be handling cases where ipt would be dropped I'll leave
// this in for now though
#[cfg(not(test))] // ploop update tests will fail without this maybe TODO: 
impl Drop for Ipt {
    fn drop(&mut self) {
        // TODO: consider adding a setting as cleaning up these rules may not be desired 
        self.cleanup_iptables_rules().expect("cleanup failed");
    }
}

#[cfg(test)]
mod tests {
    use std::io::{
        self,
        prelude::*,
    };
    use std::time::Duration;
    use std::fs;

    use super::super::settings;
    use settings::*;

    use packet_parser;
    use packet_parser::packet::types::Protocol;

    #[test]
    fn test_otp_sequence_get_from_file() {
        let filepath = "tests/test_otp_sequences.txt".to_string();
        let delete_used_line = false;
        let otp = Otp {
            on: true,
            filepath: filepath.clone(),
            delete_used_line
        };
        let otp_sequence = otp.get_otp_sequence(&filepath, delete_used_line).unwrap();
        let ports = vec![ 18455, 29399, 27823, 24656, 19332 ];
        let protocols = ports.clone().into_iter().map(|_p| Protocol::Udp).collect();
        assert_eq!(otp_sequence, Sequence {
            ports,
            seq_timeout: Duration::new(5,0),
            protocols
        })
    }

    #[test]
    fn test_otp_sequence_delete_used_line() {
        let filepath = "tests/test_otp_sequences.txt".to_string();
        let delete_used_line = true;
        let otp = Otp {
            on: true,
            filepath: filepath.clone(),
            delete_used_line
        };
        let otp_sequence = otp.get_otp_sequence(&filepath, delete_used_line).unwrap();
        let ports = vec![ 18455, 29399, 27823, 24656, 19332 ];
        let protocols = ports.clone().into_iter().map(|_p| Protocol::Udp).collect();
        assert_eq!(otp_sequence, Sequence {
            ports: ports.clone(),
            seq_timeout: Duration::new(5,0),
            protocols
        });

        let file = fs::OpenOptions::new()
            .write(true)
            .read(true)
            .open(filepath)
            .expect("unable to open file");

        let reader = io::BufReader::new(file);

        for line in reader.lines() {
            let file_ports = line.unwrap().split(',').map(|s| { 
                s.chars()
                 .filter(|c| !c.is_whitespace())
                 .collect::<String>()
            }).map(|s| s.parse::<u16>().unwrap()).collect::<Vec<u16>>();
            assert_ne!(file_ports, ports.clone());
        }

        fs::copy(format!("{}.b", otp.filepath), otp.filepath).expect("test otp delete: file copy failed");
    }

    #[test]
    fn test_otp_sequence_delete_specific_line() {
        let filepath = "tests/test_otp_sequences_delete_specific.txt".to_string();
        let ports = vec![ 29538, 58689, 20486, 38818, 18865 ];
        let delete_used_line = true;

        let file = fs::OpenOptions::new()
            .write(true)
            .read(true)
            .open(&filepath)
            .expect("unable to open file");

        let reader = io::BufReader::new(file);

        let otp = Otp {
            on: true,
            filepath: filepath.clone(),
            delete_used_line,
        };
        otp.delete_used_port_sequence(reader, &ports).expect("test_delete_specific_line failed");

        let file = fs::OpenOptions::new()
            .write(true)
            .read(true)
            .open(filepath)
            .expect("unable to open file");

        let reader = io::BufReader::new(file);

        for line in reader.lines() {
            let file_ports = line.unwrap().split(',').map(|s| { 
                s.chars()
                 .filter(|c| !c.is_whitespace())
                 .collect::<String>()
            }).map(|s| s.parse::<u16>().unwrap()).collect::<Vec<u16>>();
            assert_ne!(file_ports, ports.clone());
        }

        fs::copy(format!("{}.b", otp.filepath), otp.filepath).expect("test otp delete: file copy failed");
    }
}
