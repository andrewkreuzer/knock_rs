use super::settings;
use super::State;

use std::net::IpAddr;
use std::fmt;
use std::time::{ SystemTime, Instant };
use std::error;

use uuid::Uuid;
use log::{info, warn};
use packet_parser;
use packet_parser::packet::mac_addr::MacAddr;


#[derive(Debug, Clone)]
pub enum KnockerCmd {
    OpenSSH,
    CloseSSH,
    Unknown,
}

impl From<&str> for KnockerCmd {
    fn from(cmd: &str) -> Self {
        match cmd {
            "openSSH"  => KnockerCmd::OpenSSH,
            "closeSSH" => KnockerCmd::CloseSSH,
                    _  => KnockerCmd::Unknown,
        }
    }
}

impl KnockerCmd {
    pub fn run_cmd(

        &self,
        knocker: &Knocker,
        state: &mut State

    ) -> Result<(), Box<dyn error::Error>> {

        let uuid = Knocker::make_uuid(
            &state.session_id,
            &knocker.mac_address,
            &knocker.source_ip,
            &knocker.epoch,
        );

        if uuid != knocker.uuid {
            warn!("UUID check failed on knocker: {}", knocker);
            return Err("UUID check failed not running knocker command".into());
        }

        match self {
            KnockerCmd::OpenSSH => {
                let allowed_ip = match knocker.source_ip {
                    IpAddr::V4(ip) => ip.to_string(),
                    IpAddr::V6(ip) => ip.to_string(),
                };
                if state.ipt.ports.contains(&22) {
                    info!("Replay of open port sequence, port already open");
                } else {
                    state.ipt.open_sesame(&allowed_ip)?;
                    state.ipt.ports.push(22);
                    if state.settings.otp.on {
                        state.settings
                            .otp_sequence_update()
                            .expect("otp sequence update failed");
                    }
                }
            },
            KnockerCmd::CloseSSH => {
                if state.ipt.ports.contains(&22) {
                    state.ipt.close_the_gates()?;
                    state.ipt.ports.retain(|port| *port != 22);
                } else {
                    info!("Replay of close port sequence, port already closed");
                }
            },

            _ => {},
        }
        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct Knocker {
    pub uuid: Uuid,
    pub epoch: u64,
    pub stage: u8,
    pub mac_address: MacAddr,
    pub source_ip: IpAddr,
    pub start_time: Instant,
    pub sequence: settings::Sequence,
    pub cmd: KnockerCmd,
}

impl Knocker {
    pub fn new(

        sessions_id: Uuid,
        packet_info: &packet_parser::PacketInfo,
        cmd: KnockerCmd,
        sequence: settings::Sequence

    ) -> Knocker {

        let epoch = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();

        let uuid = Knocker::make_uuid(
            &sessions_id,
            &packet_info.mac_address,
            &packet_info.source_ip,
            &epoch
        );

        Knocker {
            uuid, 
            epoch,
            stage: 0,
            mac_address: packet_info.mac_address.clone(),
            source_ip: packet_info.source_ip,
            start_time: packet_info.start_time,
            sequence,
            cmd,
        }
    }

    pub fn is_next(&self, packet: &packet_parser::PacketInfo) -> bool {
        packet.port == self.sequence.ports[self.stage as usize]
        && packet.protocol == self.sequence.protocols[self.stage as usize]
        && self.mac_address == packet.mac_address
        && self.source_ip == packet.source_ip
    }

    pub fn is_copy(&self, packet: &packet_parser::PacketInfo) -> bool {
        let stage = if self.stage > 0 { self.stage -1 } else { 0 };

        packet.port == self.sequence.ports[stage as usize]
        && packet.protocol == self.sequence.protocols[stage as usize]
        && self.mac_address == packet.mac_address
        && self.source_ip == packet.source_ip
    }

    fn make_uuid(

        session_id: &Uuid,
        mac_address: &MacAddr,
        source_ip: &IpAddr,
        epoch: &u64

    ) -> Uuid {

        Uuid::new_v5(
            session_id, 
            (mac_address.to_string()
             + source_ip.to_string().as_str()
             + epoch.to_string().as_str()
            ).as_bytes()
        )

    }

}

impl fmt::Display for Knocker {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{{ UUID: {} MAC: {} IP: {} Time: {:?} Stage: {} }}",
            self.uuid,
            self.mac_address,
            self.source_ip,
            Instant::now().duration_since(self.start_time),
            self.stage
        )
    }
}
