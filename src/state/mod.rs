use std::sync::{Arc, Mutex};
use std::error;

use uuid::Uuid;
use log::{info, warn};

pub mod knocker;
pub mod ipt;
use ipt::Ipt;
pub mod settings;
use settings::*;
pub mod update;

// TODO: this is duplicated for Ipt::cleanup_iptables_rules() but ctrlc moves the instance of state
// which contains ipt and therefore it can no longer be used mutably within the ploop, this may be
// a case to finally learn RefCell
pub fn ctrlc_handle() {
    let ipt = iptables::new(false).unwrap();
    match ipt.delete_all("filter", "INPUT", "-j KNOCK_RS") {
        Ok(_) => info!("Removed KNOCK_RS from INPUT chain"),
        Err(e) => warn!("Failed to remove KNOCK_RS from INPUT chain: {}", e),
    }

    if ipt.chain_exists("filter", "KNOCK_RS").unwrap() {
        ipt.flush_chain("filter", "KNOCK_RS").unwrap();
        match ipt.delete_chain("filter", "KNOCK_RS") {
            Ok(_) => info!("KNOCK_RS chain deleted"),
            Err(e) => warn!("Blocking port 22 failed: {}", e),
        }
    }
    std::process::exit(130);
}

pub struct State {
    pub session_id: Uuid,
    pub ipt: Ipt,
    pub settings: Settings,
    pub knock_attempts: Arc<Mutex<Vec<knocker::Knocker>>>,
}

impl State {
    pub fn new() -> Result<State, Box<dyn error::Error>> {
        let settings = Settings::new()?;
        let ipt = Ipt::new()?;
        let knock_attempts = Arc::new(Mutex::new(Vec::<knocker::Knocker>::new()));

         Ok(State {
             session_id: Uuid::new_v4(),
             ipt,
             settings,
             knock_attempts: knock_attempts.clone(),
         })
    }
}
