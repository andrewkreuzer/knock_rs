use std::collections::HashMap;
use std::time::Duration;
use std::fs;
use std::io::{
    self,
    prelude::*,
};
use std::fs::File;

use serde::{Deserialize, Deserializer};
use simplelog::*;

use packet_parser::packet::types::Protocol;

#[derive(Debug, Deserialize)]
pub struct Otp {
    pub on: bool,
    pub filepath: String,
    pub delete_used_line: bool,
}

impl Otp {
    pub fn get_ports_from_file(&self, reader: &mut io::BufReader<fs::File>) -> io::Result<Vec<u16>> {
        let mut line = String::new();
        reader.read_line(&mut line)?;

        let ports = line.split(',').map(|s| { 
            s.chars()
            .filter(|c| !c.is_whitespace())
            .collect::<String>()
        }).map(|s| s.parse::<u16>().unwrap()).collect();

        Ok(ports)
    }

    pub fn delete_used_port_sequence(&self, mut reader: io::BufReader<fs::File>, ports: &Vec<u16>) -> io::Result<()> {
        let ports_string = ports.into_iter()
            .map(|n| n.to_string())
            .collect::<Vec<String>>()
            .join(", ");

        let top_buf = {
            let mut top_buf = String::new();
            let mut buf = String::new();
            reader.seek(io::SeekFrom::Start(0))?;

            while buf.chars().filter(|c| c != &'\n' ).collect::<String>() != ports_string {
                top_buf = format!("{}{}", top_buf, buf);
                buf.clear();
                reader.read_line(&mut buf)?;
            }
            top_buf
        };

        let bottom_buf = {
            let mut buf = Vec::new();
            reader.read_to_end(&mut buf)?;
            buf
        };

        let buf = [top_buf.as_bytes(), &bottom_buf[..]].concat();
        let mut file = reader.into_inner();
        file.seek(io::SeekFrom::Start(0))?;
        file.write_all(&buf)?;
        file.set_len(buf.len() as u64)?;

        Ok(())
    }

    pub fn get_otp_sequence(&self, filepath: &str, delete_used_line: bool) -> io::Result<Sequence> {
        let file = fs::OpenOptions::new()
            .write(true)
            .read(true)
            .open(filepath)
            .expect("unable to open file");

        let mut reader = io::BufReader::new(file);

        let ports = self.get_ports_from_file(&mut reader)?;
        if delete_used_line {
            self.delete_used_port_sequence(reader, &ports)?;

        }

        // TODO: generate and read protocols
        // println!("{:?}", ports);
        let protocols = ports.iter().map(|_p| Protocol::Udp).collect();

        Ok(Sequence {
            ports,
            seq_timeout: Duration::new(5, 0), // need to read this from config
            protocols,
        })
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Sequence {
    pub ports: Vec<u16>,
    pub seq_timeout: Duration,
    pub protocols: Vec<Protocol>,
}

impl<'de> Deserialize<'de> for Sequence {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where D: Deserializer<'de> 
    {
        
        #[derive(Debug, Deserialize)]
        struct SerdeSequence {
            ports: Vec<u16>,
            seq_timeout: u8,
            protocols: Vec<String>,
        }

        let seq = SerdeSequence::deserialize(deserializer)?;

        let seq_timeout = Duration::new(seq.seq_timeout as u64, 0);

        Ok(Sequence {
            ports: seq.ports,
            seq_timeout,
            protocols: seq.protocols
                .into_iter()
                .map(|proto| match &proto[..] {
                    "udp" => Protocol::Udp,
                    "tcp" => Protocol::Tcp,
                    _ => panic!("proto not supported"),
                })
                .collect(),
        })
    }
}

#[derive(Debug, Deserialize)]
pub struct LogSettings {
    pub on: bool,
    pub filepath: String
}

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub log: LogSettings,
    pub otp: Otp,
    pub sequences: HashMap<String, Sequence>,
}

impl Settings {
    pub fn new() -> Result<Self, io::Error> {
        let config_file: &str;
        if cfg!(debug_assertions) {
            config_file = "config/settings.toml"
        } else {
            config_file = "/etc/knock_rs/settings.toml"
        }

        let mut s = String::new();
        fs::File::open(config_file)
            .and_then(|mut f| f.read_to_string(&mut s))
            .unwrap();

        let mut settings: Settings = toml::from_str(&s)?;

        if settings.otp.on {
            settings.otp_sequence_update()?;
        }

        if settings.log.on {
            CombinedLogger::init(
            vec![
                TermLogger::new(LevelFilter::Info,
                                Config::default(),
                                TerminalMode::Mixed),

                WriteLogger::new(LevelFilter::Info,
                                  Config::default(),
                                  File::create(settings.log.filepath.clone())?),
            ]).unwrap();
        } else {
            TermLogger::init(LevelFilter::Info,
                            Config::default(),
                            TerminalMode::Mixed).unwrap();// TODO: Error handling
        }


        Ok(settings)
    }

    pub fn otp_sequence_update(&mut self) -> Result<(), io::Error> {
        if let Some(seq) = self.sequences.get_mut("openSSH") {
            *seq = self.otp.get_otp_sequence(self.otp.filepath.as_str(), 
                                              self.otp.delete_used_line)?;
        }
        Ok(())
    }
}



