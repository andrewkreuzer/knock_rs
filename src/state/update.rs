use super::{ 
    State, 
    knocker::{ Knocker, KnockerCmd }};

use std::error;
use std::sync::{Arc, Mutex};

use log::info;
use packet_parser;

pub fn update(

    packet_info: packet_parser::PacketInfo,
    state: Arc<Mutex<State>>,

    ) -> Result<(), Box<dyn error::Error>> {

    // no idea if this is a good idea. I like the handling of references to knock_attempts
    // but this does hold an arc inside an arc which is both being used mutably and immutably 
    // hence the need for the arc in arc
    let mut state = state.lock().unwrap();
    let knocker_ref = state.knock_attempts.clone();
    let mut knock_attempts = knocker_ref.lock().unwrap();
    
    //remove knocker attempts which have passsed through all stages
    // and all that have expired past the max timeout
    knock_attempts.retain(|knocker| {
        knocker.sequence.ports.len() > knocker.stage as usize
        && knocker.start_time.elapsed() < knocker.sequence.seq_timeout
    });

    let mut matched_known_knocker = false;
    for knocker in knock_attempts.iter_mut() {
        if knocker.is_next(&packet_info) {
            knocker.stage += 1;
            info!("Updated {:?} knocker: {}", knocker.cmd, knocker);
        }

        if knocker.is_copy(&packet_info) {
            matched_known_knocker = true;
        }

        if knocker.stage as usize == knocker.sequence.ports.len() {
            info!("Knocker passed through all stages: {}", knocker);

            knocker.cmd.run_cmd(&knocker, &mut state)?;
        }
    }

    if !matched_known_knocker {
        for (cmd, sequence) in state.settings.sequences.iter() {
            if packet_info.port == sequence.ports[0] &&
               packet_info.protocol == sequence.protocols[0] {
                let mut knocker = Knocker::new(
                    state.session_id,
                    &packet_info,
                    KnockerCmd::from(cmd.as_ref()),
                    sequence.clone(),
                );
                knocker.stage += 1;
                info!("Added new {:?} knocker: {}", knocker.cmd, knocker);
                knock_attempts.push(knocker);
                }
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::super::{State, ipt::Ipt, settings::{self, Settings, LogSettings}, settings::Otp, knocker::Knocker};

    use std::net::{IpAddr, Ipv4Addr};
    use std::time::{Instant, Duration};
    use std::collections::HashMap;

    use iptables::IPTables;

    use packet_parser::packet::types:: Protocol;
    use packet_parser::packet::mac_addr::{ MacAddr, MacAddrOctets };

    #[test]
    fn test_state_update() {
        let packet_info = 
            packet_parser::PacketInfo {
                mac_address: MacAddr::Octets(MacAddrOctets::new(0,0,0,0,0,0)),
                source_ip: IpAddr::V4(Ipv4Addr::new(127,0,0,1)),
                destination_ip: IpAddr::V4(Ipv4Addr::new(127,0,0,1)),
                protocol: Protocol::Udp,
                port: 1234,
                start_time: Instant::now(),
            };

        let mut sequences = HashMap::new();
        sequences.insert("test".to_string(), 
                         settings::Sequence {
                            ports: vec![1234, 1234, 1234],
                            seq_timeout: Duration::new(5, 0),
                            protocols: vec![Protocol::Udp; 3]
                         });
        let knock_attempts = vec![
            Knocker {
                uuid: uuid::Uuid::new_v4(),
                stage: 0,
                epoch: 1234,
                mac_address: MacAddr::Octets(MacAddrOctets::new(0,0,0,0,0,0)),
                source_ip: IpAddr::V4(Ipv4Addr::new(127,0,0,1)),
                start_time: Instant::now(),
                sequence: settings::Sequence {
                    ports: vec![1234, 1234, 1234],
                    seq_timeout: Duration::new(5, 0),
                    protocols: vec![Protocol::Udp; 3]
                },
                cmd: KnockerCmd::CloseSSH,
            },
            // Knocker {
            //     stage: 0,
            //     mac_address: MacAddr::new(0,0,0,0,0,0),
            //     source_ip: IpAddr::V4(Ipv4Addr::new(127,0,0,1)),
            //     start_time: Instant::now(),
            //     sequence: state::Sequence {
            //         ports: vec![1234, 1234, 1234],
            //         seq_timeout: Duration::new(5, 0),
            //         protocols: vec![Protocol::Udp; 3]
            //     },
            //     cmd: KnockerCmd::CloseSSH,
            // },
        ];

        let state = 
            State {
                session_id: uuid::Uuid::new_v4(),
                ipt: Ipt {
                    ipt: IPTables {
                        cmd: "test",
                        has_check: false,
                        has_wait: false,
                    },
                    ports: Vec::new(),
                },
                settings: Settings {
                    log: LogSettings { on: false, filepath: "".to_string() },
                    sequences,
                    otp: Otp {
                        on: false,
                        filepath: "test".to_string(),
                        delete_used_line: false,
                    }
                },
                knock_attempts: Arc::new(Mutex::new(knock_attempts.clone())),
            };

        let state = Arc::new(Mutex::new(state));

        if let Ok(()) = update(packet_info, state.clone()) {
            let state = state.lock().unwrap();
            let knock_attempts = state.knock_attempts.lock().unwrap();
            assert_eq!(knock_attempts.first().unwrap().stage, 1)
        }

    }

    #[test]
    fn test_state_no_update() {
        let packet_info = 
            packet_parser::PacketInfo {
                mac_address: MacAddr::Octets(MacAddrOctets::new(0,0,0,0,0,0)),
                source_ip: IpAddr::V4(Ipv4Addr::new(127,0,0,1)),
                destination_ip: IpAddr::V4(Ipv4Addr::new(127,0,0,1)),
                protocol: Protocol::Udp,
                port: 12345,
                start_time: Instant::now(),
            };

        let mut sequences = HashMap::new();
        sequences.insert("test".to_string(), 
                         settings::Sequence {
                            ports: vec![1234, 1234, 1234],
                            seq_timeout: Duration::new(5, 0),
                            protocols: vec![Protocol::Udp; 3]
                         });
        let knock_attempts = vec![
            Knocker {
                uuid: uuid::Uuid::new_v4(),
                epoch: 1234,
                stage: 0,
                mac_address: MacAddr::Octets(MacAddrOctets::new(0,0,0,0,0,0)),
                source_ip: IpAddr::V4(Ipv4Addr::new(127,0,0,1)),
                start_time: Instant::now(),
                sequence:settings::Sequence {
                    ports: vec![1234, 1234, 1234],
                    seq_timeout: Duration::new(5, 0),
                    protocols: vec![Protocol::Udp; 3]
                },
                cmd: KnockerCmd::CloseSSH,
            },
        ];

        let state = 
            State {
                session_id: uuid::Uuid::new_v4(),
                ipt: Ipt {
                    ipt: IPTables {
                        cmd: "test",
                        has_check: false,
                        has_wait: false,
                    },
                    ports: Vec::new(),
                },
                settings: Settings {
                    log: LogSettings { on: false, filepath: "".to_string() },
                    sequences,
                    otp: Otp {
                        on: false,
                        filepath: "test".to_string(),
                        delete_used_line: false,
                    }
                },
                knock_attempts: Arc::new(Mutex::new(knock_attempts.clone())),
            };

        let state = Arc::new(Mutex::new(state));

            // Knocker {
            //     stage: 0,
            //     mac_address: MacAddr::new(0,0,0,0,0,0),
            //     source_ip: IpAddr::V4(Ipv4Addr::new(127,0,0,1)),
            //     start_time: Instant::now(),
            //     sequence: state::Sequence {
            //         ports: vec![1234, 1234, 1234],
            //         seq_timeout: Duration::new(5, 0),
            //         protocols: vec![Protocol::Udp; 3]
            //     },
            //     cmd: KnockerCmd::CloseSSH,
            // },
        if let Ok(()) = update(packet_info, state.clone()) {
            let state = state.lock().unwrap();
            let knock_attempts = state.knock_attempts.lock().unwrap();
            assert_eq!(knock_attempts.first().unwrap().stage, 0)
        }
    }
}

