#!/usr/bin/python3
import paramiko
import time
import json
import os

def connect_SSH():
    time.sleep(10)
    client = paramiko.SSHClient()
    try:
        client.set_missing_host_key_policy(paramiko.client.AutoAddPolicy)
        client.connect('knock-server', username='knock', password='Myreally5ecurepassword', timeout=5)
        print("connected successfully")
        # stdin, stdout, stderr
        _, stdout, _ = client.exec_command('cat flag.txt')
        for line in stdout:
            flag = line.strip()
            if flag == "ya done did it":
                return True

    except paramiko.ssh_exception.SSHException as e:
        print(f"SSHException {e.args[0]}")
        return False
    except Exception as e:
        print(f"SSH Connection failed: {e}")
        return False

def run_knocker(closeSession):
    close_sequence = "8083 8084 8085"
    open_sequence = "7070 7071 7072"
    os.system(
        f"~/knock_rs/target/debug/client -r knock-server -p udp -s "
        f"{ close_sequence if closeSession else open_sequence }"
    )


def write_to_file(results):
    with open("test_results.log", 'w') as f:
        j = json.dumps(results)
        f.write(j)
    

def check_SSH():
    results = {}
    count = 0
    while not connect_SSH():
        results[count] = "failed attempt"
        run_knocker(False)
        if count >= 5:
            results['final'] = "Ending test due to 5 failed attempts"
            write_to_file(results)
            return
        count += 1

    results['final'] = "Successful connection"
    write_to_file(results)
    run_knocker(True)
    

if __name__ == '__main__':
    check_SSH()
