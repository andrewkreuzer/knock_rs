source "azure-arm" "knock" {
  client_id = var.client_id
  client_secret = var.client_secret
  subscription_id = var.subscription_id
  tenant_id = var.tenant_id

  os_type                            = "Linux"
  image_offer                        = "0001-com-ubuntu-server-focal"
  image_publisher                    = "Canonical"
  image_sku                          = "20_04-lts"
  location                           = "Canada East"
  managed_image_name                 = "knock_image"
  managed_image_resource_group_name  = "gitlab_ci_cd"

  vm_size                  = "Standard_DS2_v2"
}

build {
  sources = ["source.azure-arm.knock"]
  provisioner "shell" {
    execute_command = "chmod +x {{ .Path }}; {{ .Vars }} sudo -E sh '{{ .Path }}'"
    inline = [
      "apt-get update",
      "apt-get upgrade -y",
      "apt-get install -y build-essential python3-dev python3-pip unzip",
      "python3 -m pip install paramiko psutil"
    ]
  }
}

