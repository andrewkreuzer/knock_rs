#!/usr/bin/python3
import sys
import pprint

strace_output = sys.argv[1]
try:
    format_to_syscallz = True if sys.argv[2] == "true" else False
except:
    format_to_syscallz = False

pp = pprint.PrettyPrinter()

with open(strace_output) as f:
    syscalls = set(ln.split('(', 1)[0] for ln in f if ln[0] not in ['-', '+', '<'])
    syscalls = sorted(syscalls)
    if format_to_syscallz:
        for ln in syscalls: print(f"ctx.allow_syscall(Syscall::{ln})?;")
    else:
        pprint.pprint(set(syscalls[:480]))

