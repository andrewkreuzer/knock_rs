#!/bin/bash

file=$1

awk -F "(" '{sub(/^+.*|-.*|/, "");  print $1; }' $file | awk '!seen[$1]++' | sed -e '/^$/d'
